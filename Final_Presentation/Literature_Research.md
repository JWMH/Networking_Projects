[Mobile virtual network operator strategy for migration towards 4G](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7156473&queryText=mobile%20virtual%20network%20operator&newsearch=true)

[A Dynamic VPN Architecture for Private Cloud Computing](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6123532&queryText=cloud%20VPN&newsearch=true)

[Architecture for Inter-cloud Services Using IPsec VPN](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6168414&queryText=cloud%20VPN&ranges=2010_2016_Year)

[Dynamic IPsec VPN architecture for private cloud services](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6413486&queryText=cloud%20VPN&ranges=2010_2016_Year)

[To cloud or not to cloud: A study of trade-offs between in-house and outsourced virtual private network](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6459949&queryText=cloud%20VPN&ranges=2010_2016_Year)

[Hierarchical Policy-Based Multi-User Bandwidth Broker System Suitable for Large-Scale Bandwidth On-Demand Services](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6379692&queryText=Bandwidth%20on%20Demand&ranges=2010_2016_Year)

[A Shapley-value Mechanism for Bandwidth On Demand between Datacenters](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7274704&queryText=Bandwidth%20on%20Demand&ranges=2010_2016_Year)

[Verification and user experience of high data rate bandwidth-on-demand networks](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6404460&queryText=Bandwidth%20on%20Demand&ranges=2010_2016_Year)

[Design and Evaluation of Application-Cooperative Bandwidth On-Demand Network with Dynamic Reconfiguration for Gbps-Class HDTV Transmission](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=5962531&queryText=Bandwidth%20on%20Demand&ranges=2010_2016_Year)

[GreenCloud for Simulating QoS-Based NaaS in Cloud Computing](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6746535&queryText=NaaS&ranges=2010_2016_Year)

[An open source based network as a service (NaaS) platform for cloud provisioning](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7413641&queryText=NaaS&ranges=2010_2016_Year)

[OpenNaaS Based Management Solution for Inter-data Centers Connectivity](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6735399&queryText=NaaS&ranges=2010_2016_Year)

[Network Virtualization Platform for Hybrid Cloud](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6735398&queryText=NaaS&ranges=2010_2016_Year)

[Virtualization in VANETs to support the vehicular cloud — Experiments with the network as a service model](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6933225&queryText=Network%20as%20a%20service&newsearch=true)

[iDaaS: Inter-Datacenter Network as a Service](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7347416&queryText=Network%20as%20a%20service&newsearch=true)

[NaaS: QoS-aware Cloud Networking Services](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6623647&queryText=Network%20as%20a%20service&newsearch=true)

[Network-as-a-Service in Software-Defined Networks for end-to-end QoS provisioning](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6839919&queryText=Network%20as%20a%20service&newsearch=true)

[Media network as a service](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=6871683&queryText=Network%20as%20a%20service&newsearch=true)

[Multi-resource energy-efficient routing in cloud data centers with network-as-a-service](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7405595&queryText=Network%20as%20a%20service&newsearch=true)

[Field trial of bandwidth on demand services based on hierarchical control over multi-domain OTN networks](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7331139&queryText=Bandwidth%20on%20demand&ranges=2010_2016_Year)

[Application-network collaborative bandwidth on-demand for uncompressed HDTV transmission in IP-optical networks](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=5486579&queryText=Bandwidth%20on%20demand&ranges=2010_2016_Year)

[A Shapley-value Mechanism for Bandwidth On Demand between Datacenters](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7274704&queryText=Bandwidth%20on%20demand&ranges=2010_2016_Year)

[An SDN-based architecture for Network-as-a-Services](http://0-ieeexplore.ieee.org.read.cnu.edu/xpl/articleDetails.jsp?arnumber=7116124&action=search&sortType=&rowsPerPage=&searchField=Search_All&matchBoolean=true&queryText=%28NaaS%29)


Application-network collaborative bandwidth on-demand for uncompressed HDTV transmission in IP-optical networks
- Summary: The paper talks about the need for Bandwidth on Demand for HDTV streaming. It looks at the use of immediate BoD, reservation for future bandwidth, and planned bandwidth reservation for file transfer, for the transfer of HDTV streaming.

Mobile virtual network operator strategy for migration towards 4G
- Summary: The Mobile network operators re migrating networks over to 4G. They Suggest that Mobile virtual network operator implement some services that will reduce the cost of the communication.

OpenNaaS Based Management Solution for Inter-data Centers Connectivity
- Summary: To help network service provisioning management among Data Centers. To solve this problem they are asking to use Virtualization technologies such as OpenNaaS and OpenStack.

GreenCloud for Simulating QoS-Based NaaS in Cloud Computing
- Summary: GreenCloud is a service that looks at simulating QoS to see how well the behavior and performance of cloud based NaaS.

Hierarchical Policy-Based Multi-User Bandwidth Broker System Suitable for Large-Scale Bandwidth On-Demand Services
- Summary: This is talking about a Bandwidth Broker system to help when multiple people are using a bandwidth-on-demand. The Broker will provide flow management to help with quality control.

An open source based network as a service (NaaS) platform for cloud provisioning
- Summary: The paper looks at the problem areas in NaaS, and the multiple solutions on the open source middle area. They look at TNaaS, OpenStack cloud software, open vSwitches, and Floodlight Software Defined Network controller.

Architecture for Inter-cloud Services Using IPsec VPN
- Summary: This paper takes a look at the advantages and disadvantages of of implementing IPsec VPN within inter cloud computing architecture. This need stems of the growing need of inter connected cloud services.

Virtualization in VANETs to support the vehicular cloud — Experiments with the network as a service model
- It looks at the effects of VCC and how cloud based services can be used to overcome the limitations of VCC. It takes the VCC model and makes the vehicles to emulate the infrastructure of stationary virtual nodes.

To cloud or not to cloud: A study of trade-offs between in-house and outsourced virtual private network
- Summary: This looks at the advantages and disadvantages of using in house VPN and outsourced VPN. The conclusion is that even though there is a reduction in throughput the cloud based is preferred for easier setup.

iDaaS: Inter-Datacenter Network as a Service
- Summary: They are purposing a new type of service called iDaaS. This is serving a Inter-Datacenter Network to help cut down the cost of the service as apposed to current standing services.

After Looking at the available topics I am choosing BoD or Bandwidth on Demand for my Narrow Topic 
