W. Shi; C. Wu; Z. Li, "A Shapley-value Mechanism for Bandwidth On Demand between Datacenters," in IEEE Transactions on Cloud Computing , vol.PP, no.99, pp.1-1
doi: 10.1109/TCC.2015.2481432
keywords: {Bandwidth;Cloud computing;Computational modeling;Cost accounting;Data transfer;Pricing;Resource management;Auction Mechanism Design;Dynamic Bandwidth Pricing;Shapley Value},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=7274704&isnumber=6562694

A. Masuda, A. Isogai, K. Shiomoto, Y. Nakajima, T. Kawano and M. Maruyama, "Application-network collaborative bandwidth on-demand for uncompressed HDTV transmission in IP-optical networks," Network Operations and Management Symposium Workshops (NOMS Wksps), 2010 IEEE/IFIP, Osaka, 2010, pp. 177-180.
doi: 10.1109/NOMSW.2010.5486579
keywords: {IP networks;bandwidth allocation;high definition television;optical fibre networks;HDTV transmission;IP optical networks;application-network collaborative bandwidth on-demand network service;bandwidth reservation;network allocation;network control;ultrahigh bandwidth data transmission;Bandwidth;Board of Directors;Broadcasting;Collaboration;Delay;HDTV;Multimedia communication;Streaming media;Video compression;Video sharing},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=5486579&isnumber=5486527


C. Zhang et al., "Field trial of bandwidth on demand services based on hierarchical control over multi-domain OTN networks," in IEEE/OSA Journal of Optical Communications and Networking, vol. 7, no. 11, pp. 1057-1063, November 1 2015.
doi: 10.1364/JOCN.7.001057
keywords: {network interfaces;optical fibre networks;software defined networking;telecommunication control;telecommunication standards;BoD service;bandwidth on demand service;demand services;extended OpenFlow;hierarchical control;hierarchical software defined networking control;multidomain interoperability;multidomain optical networks;multivendor interoperability;multivendor multidomain OTN networks;optical transport network;virtual network interface;Computer architecture;Network topology;Optical fiber networks;Optimized production technology;Ports (Computers);Protocols;Topology;Bandwidth on demand; OpenFlow; Opticaltransport network; Software defined networking; Softwaredefined optical network},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=7331139&isnumber=7331118

Y. Nakajima, T. Kawano, M. Maruyama, A. Masuda, A. Isogai and K. Shiomoto, "Design and Evaluation of Application-Cooperative Bandwidth On-Demand Network with Dynamic Reconfiguration for Gbps-Class HDTV Transmission," 2011 IEEE International Conference on Communications (ICC), Kyoto, 2011, pp. 1-6.
doi: 10.1109/icc.2011.5962531
keywords: {IP networks;cooperative communication;high definition television;optical fibre networks;performance evaluation;telecommunication traffic;Gbps-class HDTV transmission;HDTV streaming;IP-optical network resource;PCE-VMTN architecture;Terabyte-class file transfer;application-cooperative BoD network;application-cooperative bandwidth on-demand network;dynamic adaptive reconfiguration;high-bandwidth data transmission;network control;path computation element;performance evaluation;traffic;video transmission;virtual network topology;Bandwidth;Board of Directors;HDTV;IP networks;Optical fiber networks;Servers;Streaming media},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=5962531&isnumber=5962409

J. Cullen, R. Hughes-Jones and R. Spencer, "Verification and user experience of high data rate bandwidth-on-demand networks," E-Science (e-Science), 2012 IEEE 8th International Conference on, Chicago, IL, 2012, pp. 1-2.
doi: 10.1109/eScience.2012.6404460
keywords: {astronomy computing;bandwidth allocation;computer network performance evaluation;field programmable gate arrays;portals;BoD;EC funded NEXPReS project;FPGA;GEANT bandwidth on demand client portal;PC based network testing tools;high data rate bandwidth-on-demand network verification;multigigabit links;report measurement;user experience;Bandwidth;Board of Directors;Educational institutions;Europe;Observatories;Real-time systems;Telescopes},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=6404460&isnumber=6404405

K. Matsui, M. Kaneda and H. Ishii, "Hierarchical Policy-Based Multi-User Bandwidth Broker System Suitable for Large-Scale Bandwidth On-Demand Services," Information and Telecommunication Technologies (APSITT), 2012 9th Asia-Pacific Symposium on, Santiago and Valparaiso, 2012, pp. 1-6.
keywords: {IP networks;quality of service;telecommunication traffic;IP traffic;QoS;carrier networks;communication quality;flow management;flow reservations;large-scale bandwidth on-demand services;multi-user interface;policy information;policy-based multi-user bandwidth broker system;quality control devices control;quality-control devices;resource information;user information;Bandwidth;Contracts;Equations;Mathematical model;Quality control;Quality of service;Scalability},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=6379692&isnumber=6379673

A. Hanemann, D. Hausheerx, P. Reiche, B. Stiller and P. van Daalen, "Investigating the economic feasibility of Bandwidth-on-Demand services for the European research networks," Network Operations and Management Symposium Workshops (NOMS Wksps), 2010 IEEE/IFIP, Osaka, 2010, pp. 201-204.
doi: 10.1109/NOMSW.2010.5486575
keywords: {IP networks;bandwidth allocation;economics;telecommunication services;European research networks;bandwidth-on-demand services;cost factors;cost-pricing model;economic feasibility;pricing models;production service;Board of Directors;Context-aware services;Costs;Economic forecasting;Ethernet networks;Internet;Jitter;Pricing;Production;Protocols},
URL: http://0-ieeexplore.ieee.org.read.cnu.edu/stamp/stamp.jsp?tp=&arnumber=5486575&isnumber=5486527
